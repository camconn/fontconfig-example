/*
 * fc-match.c - Fontconfig Matching 
 *
 * This code is released into Public Domain.
 * You may redistribute this code however you like.
 * No warranty is expressed or implied by this license
 * to the extent allowed by United States law.
 *
 * © 2020 Cameron Conn
 */

#include <stdio.h>
#include <stdlib.h>

#include <fontconfig/fontconfig.h>

int main(int argc, char **argv) {
  FcConfig*       conf;
  FcFontSet*      fs;
  FcObjectSet*    os = 0;
  FcPattern*      pat;
  FcResult        result;

  if (argc < 2) {
    fprintf(stderr, "usage: \n");
    fprintf(stderr, "    ./fc-match \"Font Name\"\n");
    return 1;
  }

  conf = FcInitLoadConfigAndFonts();
  pat = FcNameParse((FcChar8*) argv[1]);

  if (!pat)
    return 2;

  FcConfigSubstitute(conf, pat, FcMatchPattern);
  FcDefaultSubstitute(pat);

  fs = FcFontSetCreate();
  os = FcObjectSetBuild(FC_FAMILY, FC_STYLE, FC_FILE, (char*)0);

  FcFontSet *font_patterns;
  font_patterns = FcFontSort(conf, pat, FcTrue, 0, &result);

  if (!font_patterns || font_patterns->nfont == 0) {
    fprintf(stderr, "Fontconfig could not find ANY fonts on the system?\n");
    return 3;
  }

  FcPattern *font_pattern;
  font_pattern = FcFontRenderPrepare(conf, pat, font_patterns->fonts[0]);
  if (font_pattern){
    FcFontSetAdd(fs, font_pattern);
  } else {
    fprintf(stderr, "Could not prepare matched font for loading.\n");
    return 4;
  }

  FcFontSetSortDestroy(font_patterns);
  FcPatternDestroy(pat);

  if (fs) {
    if (fs->nfont > 0) {
      FcValue v;
      FcPattern *font;

      font = FcPatternFilter(fs->fonts[0], os);

      FcPatternGet(font, FC_FILE, 0, &v);
      const char* filepath = (char*)v.u.f;
      printf("path: %s\n", filepath);

      FcPatternDestroy(font);
    }
    FcFontSetDestroy(fs);
  } else {
    fprintf(stderr, "could not obtain fs\n");
  }

  if (os)
    FcObjectSetDestroy(os);

  return 0;
}
