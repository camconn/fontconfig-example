
CC = gcc
CFLAGS = -Wall -Werror `pkg-config --cflags fontconfig`
INCLUDES = `pkg-config --libs fontconfig`

fc-match: fc-match.o
	${CC} ${CFLAGS} ${INCLUDES} -o $@ $<


%.o: %.c
	${CC} -c ${CFLAGS} ${INCLUDES} -o $@ $<

clean:
	rm -rf *.o fc-match

