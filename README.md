# Fontconfig Usage Example
An example of how to use use Fontconfig with C.

Refer to my [blog post](https://www.camconn.cc/post/how-to-fontconfig-lib-c/)
on the topic.

## Running
After successfully building the program run `./fc-match <query>
```
./fc-match Helvetica
./fc-match Helvetica:Italic
./fc-match "DejaVu Sans:Bold"
./fc-match "DejaVu Sans:Bold:Italic"
```

## Building
```
make clean
make
```

## License
No warranty is expressed or implied with this code.
This code is released into the public domain.
